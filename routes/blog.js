const express = require('express');
const router = express.Router();

const reader = require('../reader');

/* GET List Blog Items. */
router.get('/', (req, res) => {
  reader(function (data) {
    res.render('blog', {
      description: "Welcome to my hey-world",
      header: data.head['title']+"@ ",
      subheader: "Hey World",
      logo: "images/icon.png",
      initial: "M",
      entries: data.items.map(item => {
        return {
          title: item['title'],
          date: item['date'].toDateString(),
          link: item['link'],
          content: item['description'],
          author_name: item['atom:author']['name']['#'],
          author_email: item['atom:author']['email']['#'],
        }
      })
    });
  });
});

module.exports = router;
