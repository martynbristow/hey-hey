// hey-hey: Display Highlights from a hey feed
require('dotenv').config()

const express = require('express')

const reader = require('./reader');

const path = require('path');
const app = express()
const port = process.env.PORT
const name = process.env.USER

app.use(express.static('public'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.get('*', (req, res) => {
  reader(function (data) {
    res.render('blog', {
      description: "Welcome to my hey-world",
      header: data.head['title']+"@ ",
      subheader: "Hey World",
      logo: "images/icon.png",
      initial: "M",
      entries: data.items.map(item => {
        return {
          title: item['title'],
          date: item['date'].toDateString(),
          link: item['link'],
          content: item['description'],
          author_name: item['atom:author']['name']['#'],
          author_email: item['atom:author']['email']['#'],
        }
      })
    });
  });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
