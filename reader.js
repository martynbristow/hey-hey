// Reader for RSSFeed
const fetch = require('node-fetch');
const FeedParser = require('feedparser');
const name = "martyn"
const url = `https://world.hey.com/${name}/feed.atom`

const metaNames = {
    title: true,
    link: true,
    updated: true,
    description: false,
    pubDate: false,
    language: false,
    copyright: false,
    generator: false,
    cloud: false,
    image: false,
    categories: false
};

function feed_read(callback) {
    const f_req = fetch(url)
    const feedparser = new FeedParser();
    var theFeed = {
        head: new Object (),
        items: new Array ()
    };

    f_req.then(function (f_res) {
        if (f_res.status !== 200) {
            console.error('Bad status code');
            throw new Error('Bad status code');
        }
        else {
            f_res.body.pipe(feedparser);
        }
    }, function (err) {
        // handle any request errors
        return null;
    });

    feedparser.on('error', function (error) {
        // always handle errors
        return null;
    });

    feedparser.on('readable', function () {
        // This is where the action is!
        // const stream = this; // `this` is `feedparser`, which is a stream
        // const meta = this.meta; // **NOTE** the "meta" is always available in the context of the feedparser instance

        try {
            var item = this.read ();
            if (item !== null) {
                theFeed.items.push (item);
                for (var x in item.meta) {
                    if (metaNames [x] !== undefined) {
                        theFeed.head [x] = item.meta [x];
                    }
                }
            }
        }
        catch (err) {
            console.log ("parseFeedString: err.message == " + err.message);
        }
    });
        // data['author'] = meta['title']
        // data['reply'] = meta['title']
        // data['url'] = meta['link']
        // data['updated'] = meta['date']
        // data['entries'] = Array();
        //
        // // Process a Blog Entry
        // var entry;
        // while (entry = stream.read()) {
        //     data['entries'].push(entry['title'])
        // }
    // })
    feedparser.on('end', function () {
        callback(theFeed)
    });

}

module.exports = feed_read